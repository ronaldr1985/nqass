package main

import (
	"bufio"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log"
	"net/http"
	"os"
	"os/exec"
	"os/user"
	"path"
	"regexp"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/pborman/getopt"
)

// Global Variables
var distro, package_manager, username, refresh_repos_args, update_args, install_args, enable_service_command_format, add_group_command_format, make_executable string

// enable_service_command_format and add_group_command_format are variables that have the command in the following format:
// addgroup %s %s
// This is done to make passing the group or service easier.
var folders_to_be_created_in_home_folder = [...]string{"dox", "repos", "src", "src/not-mine", "src/mine", "pix", "pix/screenshots", "music", "vids", "images", ".local", ".local/zsh", "dl", "dl/fonts", ".fonts"}
var CC = "clang" // The C compiler we will use if we compile any applications

func print_option_information() {
	fmt.Println("Options: ")
	fmt.Println("---")
	fmt.Println("kvm")
	fmt.Println("mc-programming")
	fmt.Println("---")
}

func print_fatal_error(err ...any) {
	fmt.Fprintln(os.Stderr, err...)
	os.Exit(1)
}

func run_cmd_as_user(exe, args, directory, username string) (string, error) {
	var out []byte
	var cmd *exec.Cmd

	if len(args) != 0 {
		args_array := strings.Split(args, " ")
		cmd = exec.Command(exe, args_array...)
	} else {
		cmd = exec.Command(exe)
	}

	cmd.Env = os.Environ()
	cmd.Env = append(cmd.Env, "CC="+CC)
	cmd.Dir = directory

	usr, err := user.Lookup(username)
	if err != nil {
		return "", errors.New("Failed to lookup information about the user: " + username)
	}
	uid, err := strconv.Atoi(usr.Uid)
	if err != nil {
		return "", errors.New("Failed to get user ID for user: " + username)
	}
	gid, err := strconv.Atoi(usr.Gid)
	if err != nil {
		return "", errors.New("Failed to get group ID for user: " + username)
	}

	cmd.SysProcAttr = &syscall.SysProcAttr{}
	cmd.SysProcAttr.Credential = &syscall.Credential{Uid: uint32(uid), Gid: uint32(gid)}

	out, err = cmd.CombinedOutput()

	return string(out), err
}

func get_linux_distro() (string, bool) {
	var current_line string
	var distro_name string

	os_release, err := os.Open("/etc/os-release")
	if err != nil {
		return "", false
	}
	defer os_release.Close()

	r, err := regexp.Compile("^NAME=.*")
	if err != nil {
		return "", false
	}
	scanner := bufio.NewScanner(os_release)
	for scanner.Scan() {
		current_line = scanner.Text()
		if matched := r.MatchString(current_line); err != nil {
			return "", false
		} else if matched {
			i := strings.Index(current_line, `"`)
			if i < 1 {
				return "", false
			}
			distro_name = current_line[i : len(current_line)-1]
			break
		}
	}

	distro_name = strings.ReplaceAll(distro_name, "\"", "")

	return distro_name, true
}

func install_font_from_link(font_name, download_link, font_family_name, font_install_folder string) error {
	var font_install_folder_with_trailing_slash string
	if font_install_folder[len(font_install_folder)-1] != '/' {
		font_install_folder_with_trailing_slash = font_install_folder + "/"
	} else {
		font_install_folder_with_trailing_slash = font_install_folder
	}
	var font_download_file = "/home/" + username + "/dl/fonts/"

	filename := path.Base(download_link)
	filename_with_path := font_download_file + filename

	out, err := os.Create(filename_with_path)
	if err != nil {
		return err
	}
	defer out.Close()

	resp, err := http.Get(download_link)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	var font_folder_name string
	if strings.Contains(filename, ".zip") {
		font_folder_name = strings.ReplaceAll(filename_with_path, ".zip", "")
		if _, err := os.Stat(font_folder_name); errors.Is(err, os.ErrNotExist) {
			err := os.Mkdir(font_folder_name, os.ModePerm)
			if err != nil {
				return err
			}
		}

		unzip_command_args := filename_with_path + " -d" + font_folder_name
		output, err := run_cmd_as_user("unzip", unzip_command_args, font_download_file, "root")
		if err != nil {
			return errors.New(output)
		}
	}

	lower_case_font_name := strings.ToLower(font_name)
	lower_case_font_family_name := strings.ToLower(font_family_name)
	files, err := os.ReadDir(font_folder_name)
	if err != nil {
		return err
	}
	for _, file := range files {
		if file.IsDir() {
			sub_files, err := os.ReadDir(font_folder_name)
			if err != nil {
				return err
			}
			for _, sub_file := range sub_files {
				filename_with_complete_path := font_folder_name + file.Name() + "/" + sub_file.Name()
				lower_case_sub_file_name := strings.ToLower(sub_file.Name())
				if strings.Contains(lower_case_sub_file_name, lower_case_font_name) || strings.Contains(lower_case_sub_file_name, lower_case_font_family_name) {
					err := os.Rename(filename_with_complete_path, font_install_folder_with_trailing_slash+sub_file.Name())
					if err != nil {
						return err
					}
				}
			}
		} else {
			filename_with_complete_path := font_folder_name + "/" + file.Name()
			lower_case_file_name := strings.ToLower(file.Name())
			if strings.Contains(lower_case_file_name, lower_case_font_name) || strings.Contains(lower_case_file_name, lower_case_font_family_name) {
				err := os.Rename(filename_with_complete_path, font_install_folder_with_trailing_slash+file.Name())
				if err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func copy_file(source, destination string) error {
	stat, err := os.Stat(source)
	if err != nil {
		return err
	} else if !stat.Mode().IsRegular() {
		return errors.New("Source file is not a regular file")
	}

	source_file, err := os.Open(source)
	if err != nil {
		return err
	}
	defer source_file.Close()

	destination_file, err := os.Create(destination)
	if err != nil {
		return err
	}
	defer destination_file.Close()

	_, err = io.Copy(destination_file, source_file)
	return err
}

func install_config_file(program_name, config_file_location, config_file_install_location, permissions, owner, group string) error {
	err := copy_file(config_file_location, config_file_install_location)
	if err != nil {
		return errors.New("Failed to copy file")
	}
	permissions_uint, err := strconv.ParseUint(permissions, 8, 32)
	if err != nil {
		return errors.New("Invalid permissions in config.csv " + permissions)
	}
	err = os.Chmod(config_file_install_location, fs.FileMode(permissions_uint))
	if err != nil {
		return err
	}

	usr, err := user.Lookup(owner)
	if err != nil {
		return errors.New("Failed to lookup information about the user: " + username)
	}
	grp, err := user.LookupGroup(group)
	if err != nil {
		return errors.New("Failed to lookup information about the group: " + group)
	}
	uid, err := strconv.Atoi(usr.Uid)
	if err != nil {
		return errors.New("Failed to get user ID for user: " + username)
	}
	gid, err := strconv.Atoi(grp.Gid)
	if err != nil {
		return errors.New("Failed to get group ID for group: " + group)
	}
	err = os.Chown(config_file_install_location, uid, gid)
	if err != nil {
		return err
	}

	return nil
}

func set_user_password(username, password string) bool {
	yes := exec.Command("yes", password)
	passwd := exec.Command("passwd", username)

	// Get the output of the first command and connect it as input to the second command
	passwd.Stdin, _ = yes.StdoutPipe()

	// Get the output of the second command
	passwdStdout, _ := passwd.StdoutPipe()

	// Start both commands
	_ = passwd.Start()
	_ = yes.Start()

	// Read the output of the second command
	_, err := io.ReadAll(passwdStdout)
	if err != nil {
		return false
	}

	// Wait for passwd command to finish
	_ = passwd.Wait()

	return true
}

func main() {
	var ok bool
	var home_folder, nqass_config_file, app_src_folder, current_src_folder, patches_folder, font_install_folder, csv_distro,
		csv_install_method, csv_package_manager_additional_args, csv_name, csv_option, csv_url, csv_folder, csv_patch,
		csv_font_family, csv_config_file_location, csv_config_file_install_location, csv_config_file_permissions,
		csv_config_file_owner, csv_config_file_group, zsh_full_path string

	if distro, ok = get_linux_distro(); !ok {
		print_fatal_error("Failed to get distro")
	} else if distro == "Void" {
		package_manager = "xbps-install"
		refresh_repos_args = "-Sy"
		update_args = "-Syu"
		install_args = "-y"
		enable_service_command_format = "ln -s /etc/sv/%s /var/service/"
		add_group_command_format = "usermod -aG %s %s"
	} else if distro == "Alpine Linux" {
		package_manager = "apk"
		refresh_repos_args = "update"
		update_args = "upgrade"
		install_args = "add"
		enable_service_command_format = "rc-update add %s"
		add_group_command_format = "usermod -aG %s %s"
	} else if distro == "Debian GNU/Linux" {
		package_manager = "apt"
		refresh_repos_args = "update"
		update_args = "upgrade -y"
		install_args = "install -y"
		enable_service_command_format = "systemctl enable %s"
		add_group_command_format = "usermod -aG %s %s"
	} else if distro == "Ubuntu" {
		package_manager = "apt"
		refresh_repos_args = "update"
		update_args = "upgrade -y"
		install_args = "install -y"
		enable_service_command_format = "systemctl enable %s"
		add_group_command_format = "usermod -aG %s %s"
	} else {
		print_fatal_error("Sorry your distribution is not supported")
	}

	opt_user := getopt.StringLong("user", 'u', "", "The user that you will be using after nqass is finished [Required]")
	opt_root_password := getopt.StringLong(
		"root-password",
		'r',
		"",
		"The new root password to be set",
	)
	opt_options := getopt.StringLong("options", 'o', "", "Which additional pieces of software you would like installed")
	opt_information := getopt.BoolLong(
		"information",
		'i',
		"Information about the additional software the nqass can install",
	)
	opt_help := getopt.BoolLong("help", 0, "Help")
	getopt.Parse()

	if len(getopt.Args()) < 1 {
		fmt.Fprintln(os.Stderr, "Was not passed a configuration file")
		print_fatal_error("Please pass a configuration file to the script, e.g. sudo ./nqass config.csv")
	}
	args := getopt.Args()
	nqass_config_file = args[0]

	if *opt_information {
		print_option_information()
		os.Exit(0)
	}
	if *opt_help {
		getopt.Usage()
		os.Exit(0)
	}

	set_root_password := false
	if len(*opt_root_password) > 0 {
		set_root_password = true
	}

	if len(*opt_user) == 0 {
		print_fatal_error("No user was passed to the program")
	}

	username = strings.Trim(*opt_user, " ")
	home_folder = fmt.Sprintf("/home/%s/", username)

	cwd, err := os.Getwd()
	if err != nil {
		print_fatal_error("Failed to get the current working directory")
	}

	patches_folder = fmt.Sprintf("%s/patches/", cwd)
	font_install_folder = home_folder + ".fonts/"

	fmt.Println("---")
	fmt.Printf("User: \"%s\"\n", username)
	fmt.Printf("Home folder: \"%s\"\n", home_folder)
	fmt.Printf("Folders: \"%v\"\n", folders_to_be_created_in_home_folder)
	fmt.Printf("Distro: \"%s\"\n", distro)
	fmt.Printf("Update command: \"%s %s\"\n", package_manager, update_args)
	fmt.Printf("Install package command: \"%s %s\"\n", package_manager, install_args)
	fmt.Printf("Enable service command: \"%s service\"\n", enable_service_command_format)
	fmt.Printf("Add group command: \"%s group %s\"\n", add_group_command_format, username)
	fmt.Println("Change root password:", set_root_password)
	fmt.Println("NQASS Config File:", nqass_config_file)
	fmt.Println("---")
	fmt.Println("Options:")
	fmt.Printf("KVM: %t\n", strings.Contains(strings.ToLower(*opt_options), "kvm"))
	fmt.Printf("MC-Programming: %t\n", strings.Contains(strings.ToLower(*opt_options), "mc-programming"))
	fmt.Printf("---\n\n")
	time.Sleep(5 * time.Second)

	fmt.Println("Creating folders")
	usr, err := user.Lookup(username)
	if err != nil {
		print_fatal_error("Failed to get information about user: " + username + " got the following error:\n\t" + err.Error())
	}
	uid, _ := strconv.Atoi(usr.Uid)
	gid, _ := strconv.Atoi(usr.Gid)
	for _, folder := range folders_to_be_created_in_home_folder {
		fmt.Println("Creating folder:", folder)
		if _, err := os.Stat(folder); os.IsNotExist(err) {
			if err := os.Mkdir(home_folder+folder, os.ModePerm); err != nil {
				fmt.Fprintln(os.Stderr, "Failed to create folder,", folder, "with the following error:\n", err.Error())
			}
			err = os.Chown(home_folder+folder, uid, gid)
			if err != nil {
				fmt.Fprintln(os.Stderr, "Failed to change ownership of folder,", folder, "with the following error:\n", err.Error())
			}
		}
	}
	fmt.Println("")

	if set_root_password {
		fmt.Println("Setting root password")
		ok := set_user_password("root", *opt_root_password)
		if !ok {
			fmt.Fprintln(os.Stderr, "Failed to set root password")
		}
	}

	fmt.Println("Updating the system before we get started")
	fmt.Println("Refreshing the repositories")
	if output, err := run_cmd_as_user(package_manager, refresh_repos_args, home_folder, "root"); err != nil {
		fmt.Fprintln(os.Stderr, "nqass: failed to update distribution repositories, the output of the command as:\n", output)
		os.Exit(1)
	}
	fmt.Println("Upgrading packages")
	if output, err := run_cmd_as_user(package_manager, update_args, home_folder, "root"); err != nil {
		fmt.Fprintln(os.Stderr, "nqass: failed to update distribution, the output of the command was:\n", output)
		os.Exit(1)
	}

	progs, err := os.ReadFile(nqass_config_file)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to open config file")
		os.Exit(1)
	}
	r := csv.NewReader(strings.NewReader(string(progs)))
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		csv_distro = record[0]
		csv_install_method = record[1]
		csv_package_manager_additional_args = record[2]
		csv_name = record[3]
		csv_option = record[4]
		csv_url = record[5]
		csv_folder = record[6]
		csv_patch = record[7]
		csv_font_family = record[8]
		csv_config_file_location = record[9]
		csv_config_file_install_location = record[10]
		csv_config_file_permissions = record[11]
		csv_config_file_owner = record[12]
		csv_config_file_group = record[13]

		current_src_folder = fmt.Sprintf("%s%s/", home_folder, csv_folder)
		if current_src_folder[len(current_src_folder)-1:] == "/" {
			app_src_folder = fmt.Sprintf("%s%s/", current_src_folder, csv_name)
		} else {
			app_src_folder = fmt.Sprintf("%s/%s/", current_src_folder, csv_name)
		}

		if csv_distro == distro {
			if len(csv_option) == 0 || strings.Contains(strings.ToLower(*opt_options), csv_option) {
				if strings.Contains(package_manager, csv_install_method) {
					fmt.Println("Installing package:", csv_name)
					if len(csv_package_manager_additional_args) > 0 {
						if output, err := run_cmd_as_user(package_manager, install_args+" "+csv_package_manager_additional_args+" "+csv_name, home_folder, "root"); err != nil {
							fmt.Fprintln(os.Stderr, "nqass: failed to install ", csv_name, " with the exit code: ", err)
							fmt.Fprintln(os.Stderr, "the output of the install command was:\n", output)
							os.Exit(1)
						}
					} else {
						if output, err := run_cmd_as_user(package_manager, install_args+" "+csv_name, home_folder, "root"); err != nil {
							fmt.Fprintln(os.Stderr, "nqass: failed to install ", csv_name, " with the exit code: ", err)
							fmt.Fprintln(os.Stderr, "the output of the install command was:\n", output)
							os.Exit(1)
						}
					}
				} else if csv_install_method == "service" {
					fmt.Println("Enabling the service:", csv_name)
					enable_service_command := fmt.Sprintf(enable_service_command_format, csv_name)
					first_space_pos := strings.Index(enable_service_command, " ")
					if output, err := run_cmd_as_user(enable_service_command[:first_space_pos], enable_service_command[first_space_pos+1:], home_folder, "root"); err != nil {
						print_fatal_error("nqass: failed to enable service:\n", output)
					}
				} else if csv_install_method == "group" {
					fmt.Println("Adding", username, "to the group", csv_name)
					add_group_command := fmt.Sprintf(add_group_command_format, csv_name, username)
					first_space_pos := strings.Index(add_group_command, " ")
					if output, err := run_cmd_as_user(add_group_command[:first_space_pos], add_group_command[first_space_pos+1:], home_folder, "root"); err != nil {
						print_fatal_error("nqass: failed to add ", username, " to the group: ", csv_name, " with the following error:\n", output)
					}
				}
			}
		} else if csv_distro == "Any" {
			if csv_install_method == "Make" {
				if _, err := os.Stat("/bin/bmake"); err == nil {
					make_executable = "bmake"
				} else if errors.Is(err, os.ErrNotExist) {
					make_executable = "make"
				}
				fmt.Println("Cloning the git repository for", csv_name)
				if output, err := run_cmd_as_user("git", "clone "+csv_url, current_src_folder, username); err != nil {
					fmt.Fprintln(os.Stderr, "failed to clone the repository ", csv_url, ":\n", output)
				}
				if len(csv_patch) != 0 {
					fmt.Println("Applying a patch to", csv_name)
					_, err := run_cmd_as_user("git", "apply "+patches_folder+csv_patch, app_src_folder, username)
					if err != nil {
						fmt.Fprintln(os.Stderr, "failed to apply patch", csv_patch)
					}
				}
				fmt.Println("Attempting to compile and install", csv_name)
				if _, err := run_cmd_as_user(make_executable, "clean install", app_src_folder, "root"); err != nil {
					// TODO: Add a print command to say successfully compiled and installed program
					fmt.Fprintln(os.Stderr, "failed to run \""+make_executable, "clean install\", attempting to just compile the code")
					if output, err := run_cmd_as_user(make_executable, "", app_src_folder, username); err != nil {
						fmt.Fprintln(os.Stderr, "failed to compile: ", csv_name, "with the following error:\n", output)
					}
				}
			} else if csv_install_method == "dot-files" {
				fmt.Println("Installing dot-files")
				if _, err := run_cmd_as_user("git", "clone "+csv_url, current_src_folder, username); err != nil {
					fmt.Fprintln(os.Stderr, "nqass: failed to clone the repository for ", csv_name)
				}
				if _, err := run_cmd_as_user("sudo", "-u "+username+" /bin/sh dot-restore", app_src_folder, "root"); err != nil {
					fmt.Fprintln(os.Stderr, "failed to run ./dot-restore script")
				}
			} else if csv_install_method == "font-link" {
				fmt.Println("Installing font:", csv_name)
				err := install_font_from_link(csv_name, csv_url, csv_font_family, font_install_folder)
				if err != nil {
					print_fatal_error("nqass: failed to install font with the following error:\n", err.Error())
				}
			} else if csv_install_method == "config-file" {
				fmt.Println("Installing config file for:", csv_name)
				err := install_config_file(csv_name, csv_config_file_location, csv_config_file_install_location, csv_config_file_permissions, csv_config_file_owner, csv_config_file_group)
				if err != nil {
					print_fatal_error("nqass: failed to install config file with the following error:\n", err.Error())
				}
			}
		}
	}

	possible_zsh_locations := []string{"/usr/bin/zsh", "/usr/local/bin/zsh", "/bin/zsh"}
	for _, zsh_path := range possible_zsh_locations {
		if _, err := os.Stat(zsh_path); err == nil {
			zsh_full_path = zsh_path

			break
		}
	}

	fmt.Println("Changing the shell to zsh for", username)
	if _, err := run_cmd_as_user("chsh", "-s "+zsh_full_path+" "+username, home_folder, "root"); err != nil {
		fmt.Fprintln(os.Stderr, "nqass: failed to change user's shell to zsh")
	}
	fmt.Println()

	fmt.Println("Changing permissions of ", username+"'s home folder")
	if output, err := run_cmd_as_user("chown", "-R "+username+":"+username+" "+home_folder, "/home", "root"); err != nil {
		print_fatal_error("nqass: failed to change permissions with the following error:\n", output)
	}

	var reboot string
	fmt.Printf("Would you like to reboot (y/n)? ")
	fmt.Scanf("%s", &reboot)
	for {
		if reboot == "y" {
			run_cmd_as_user("reboot", "now", "/", "root")
			fmt.Println("Rebooting...")
			break
		} else if reboot == "n" {
			fmt.Println("Exiting...")
			os.Exit(0)
		} else {
			fmt.Printf("Would you like to reboot (y/n)? ")
			fmt.Scanf("%s", &reboot)
		}
	}
}
